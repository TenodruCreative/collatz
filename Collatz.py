#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """

    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

#cache = {1: 1, 9: 20, 10: 7, 15: 18, 25: 24, 50: 25, 100: 26, 150: 16, 200: 27, 500: 111, 750: 47, 1000: 112}


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    # Define a set cache ahead of time, following meta optimization.
    cache = {1: 1, 9: 20, 10: 7, 15: 18, 25: 24, 50: 25,
             100: 26, 150: 16, 200: 27, 500: 111, 750: 47, 1000: 112}
    maxLength = 0
    # Pre-condition assertions.
    assert i <= j
    assert i > 0
    assert isinstance(i, int)
    assert j > 0
    assert isinstance(j, int)

    # Calculation of max cycle length.
    for x in range(i, j+1):
        calcRange = range(i, j+1)
        n = str(x)
        cycle = [x]

        # Cycle length calculation for each value x in [i, j].
        # Stop calculation if x is found in cache (calculation has reached a value whose cycle
        #   length is already known).
        while x not in cache:
            # x is even. Run calculation and set x to resulting value.
            if x % 2 == 0:
                x = x / 2
            # x is odd. Run calculation and set x to resulting value.
            else:
                x = 3 * x + 1
            # After calc step, append x to cycle array. Cycle contains calculation path from beginning to end.
            cycle.append(int(x))

        # Once cycle is finished, enumerate over the reversed array (end to beginning).
        # cycle[] will always end in a known integer in the cache. We start at the end of cycle[] to preserve
        #   the known cycle length for the ending value (count = 0).
        # Example: n = 5.
        #   cycle[] for n is [5, 16, 8, 4]. cycle[] ends at 4, not 1, because 4 is known in the cache from
        #       evaluating 4 previously.
        #   Reversed cycle[] is [4, 8, 16, 5].
        #   count begins at 0. Loop starts at the first index - 4. cache[4] is "recalculated," but because
        #       count = 0, cache[4] remains the same.
        #   Next is 8. 8 is just 1 step up from 4, and with count now at 1, we add 1 to the cycle length of
        #       4 (taken from the cache) to get the cycle length of 8.
        #   This repeats for each value in cycle[] until each one, and their corresponding cycle length, is
        #       added to the cache.
        for count, value in enumerate(reversed(cycle)):
            cache[value] = cache[x] + count
            # If current value is within calcRange, and the corresponding cycle length
            #   is greater than maxLength, set maxLength to this value's cycle length.
            if (value in calcRange) and (cache[value] > maxLength):
                maxLength = cache[value]

    # Post-condition assertions.
    assert maxLength > 0
    assert isinstance(maxLength, int)
    return maxLength

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
